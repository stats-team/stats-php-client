<?php

namespace StatsClient;

class Api
{
	const API_URL = 'app.cuidame.com.ar';

	protected static function makeRequest($uri, $params)
	{
		foreach ($params as $key => &$val) {
			if (is_array($val)) $val = implode(',', $val);
			$post_params[] = $key.'='.urlencode($val);
		}

		$post_string = implode('&', $post_params);

		$fp = fsockopen("tls://".self::API_URL, 443, $errno, $errstr, 30);

		if (!$fp) {
		    echo "$errstr ($errno)<br />\n";
		} else {
		    $out = "POST ".$uri." HTTP/1.1\r\n";
			$out.= "Host: ".self::API_URL."\r\n";
			$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
			$out.= "Content-length: ". strlen($post_string) ."\r\n";
			$out.= "Connection: Close\r\n\r\n";

			if (isset($post_string)) $out.= $post_string . "\r\n\r\n";

		    fwrite($fp, $out);
		    fclose($fp);
		}
	}

	public static function sendStatValue($userkey, $name, $value, $metadata = null)
	{
		self::makeRequest("/stats", ['apitoken' => $userkey, 'name' => $name, 'value' => $value, 'type' => 'value', 'metadata' => is_null($metadata) ? null : json_encode($metadata)]);
	}

	public static function sendStatCounter($userkey, $name, $counter, $metadata = null)
	{
		self::makeRequest("/stats", ['apitoken' => $userkey, 'name' => $name, 'value' => $counter, 'type' => 'counter', 'metadata' => is_null($metadata) ? null : json_encode($metadata)]);
	}
}